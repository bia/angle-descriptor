package plugins.stef.roi.descriptor;

import icy.roi.ROI;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;
import icy.type.point.Point3D;

import java.awt.geom.Point2D;
import java.util.List;

import plugins.kernel.roi.roi2d.ROI2DPolyLine;
import plugins.kernel.roi.roi3d.ROI3DPolyLine;

/**
 * Angle ROI descriptor class, work only for 3 pts PolyLine descriptor(see {@link ROIDescriptor})
 * 
 * @author Stephane
 */
public class ROIAngleDescriptor extends ROIDescriptor
{
    public static final String ID = "Angle";

    public ROIAngleDescriptor()
    {
        super(ID, "Angle", Double.class);
    }

    @Override
    public String getUnit(Sequence sequence)
    {
        return "°";
    }

    @Override
    public String getDescription()
    {
        return "Angle";
    }

    @Override
    public Object compute(ROI roi, Sequence sequence) throws UnsupportedOperationException
    {
        return Double.valueOf(computeAngle(roi));
    }

    /**
     * Returns the angle in degree given by the first 3 points of the specified ROI.
     * 
     * @param roi
     *        the ROI on which we want to compute the angle
     * @return the angle in degree given by the first 3 points of the specified ROI.
     */
    public static double computeAngle(ROI roi)
    {
        // 2D angle
        if (roi instanceof ROI2DPolyLine)
        {
            final List<Point2D> points = ((ROI2DPolyLine) roi).getPoints();

            // we calculate angle only for 3 pts ROI
            if (points.size() == 3)
            {
                final Point2D ptA = points.get(0);
                final Point2D ptB = points.get(1);
                final Point2D ptC = points.get(2);

                return Math.toDegrees(Math.atan2(ptC.getX() - ptB.getX(), ptC.getY() - ptB.getY())
                        - Math.atan2(ptA.getX() - ptB.getX(), ptA.getY() - ptB.getY()));
            }
        }

        // 3D angle
        if (roi instanceof ROI3DPolyLine)
        {
            final List<Point3D> points = ((ROI3DPolyLine) roi).getPoints();

            // we calculate angle only for 3 pts ROI
            if (points.size() == 3)
            {
                final Point3D ptA = points.get(0);
                final Point3D ptB = points.get(1);
                final Point3D ptC = points.get(2);

                return ptB.angle(ptA, ptC);
            }
        }

        // not supported
        throw new UnsupportedOperationException("Can't process '" + ID + "' calculation for ROI: " + roi.getName());
    }
}
