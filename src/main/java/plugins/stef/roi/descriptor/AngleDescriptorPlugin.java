/**
 * 
 */
package plugins.stef.roi.descriptor;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginROIDescriptor;
import icy.roi.ROI;
import icy.roi.ROIDescriptor;
import icy.sequence.Sequence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Angle descriptor for 3 points Polyline ROI.
 * 
 * @author Stephane
 */
public class AngleDescriptorPlugin extends Plugin implements PluginROIDescriptor
{
    public static final String ID_ANGLE = ROIAngleDescriptor.ID;

    public static final ROIAngleDescriptor angleDescriptor = new ROIAngleDescriptor();

    @Override
    public List<ROIDescriptor> getDescriptors()
    {
        final List<ROIDescriptor> result = new ArrayList<ROIDescriptor>();

        result.add(angleDescriptor);

        return result;
    }

    @Override
    public Map<ROIDescriptor, Object> compute(ROI roi, Sequence sequence) throws UnsupportedOperationException
    {
        final Map<ROIDescriptor, Object> result = new HashMap<ROIDescriptor, Object>();

        try
        {
            result.put(angleDescriptor, angleDescriptor.compute(roi, sequence));
        }
        catch (UnsupportedOperationException e)
        {
            result.put(angleDescriptor, null);
        }

        return result;
    }
}
